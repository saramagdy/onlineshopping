<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group([ 'prefix' => 'item','middleware' => ['auth', 'admin']], function()
{
    Route::get('create', 'ItemsController@create')->name('item.create');
    Route::post('store', 'ItemsController@store')->name('item.store');
    Route::get('edit/{id}', 'ItemsController@edit')->name('item.edit');
    Route::post('update/{id}', 'ItemsController@update')->name('item.update');
    Route::delete('delete/{id}', 'ItemsController@destroy')->name('item.delete');
    
});

Route::group([ 'prefix' => 'item','middleware' => ['auth']], function()
{
    Route::get('list', 'ItemsController@index')->name('item.list');
    
});

Route::group([ 'prefix' => 'order','middleware' => ['auth']], function()
{
   
    Route::post('store', 'OrdersController@store')->name('order.store');
    Route::get('checkout/{id}', 'OrdersController@checkout')->name('order.checkout');
    Route::get('bill', 'OrdersController@bill')->name('order.bill');
    Route::get('list', 'OrdersController@index')->name('order.list');
    
});

