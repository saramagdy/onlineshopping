@extends('layouts.app')

@section('content')
<div class="container">
<ul class="list-group">
  <li class="list-group-item">TOTAL PRICE : {{$order->total_price}}</li>
  <li class="list-group-item"><a class="btn btn-primary btn-make-order" href="{{route('order.checkout', $order->id )}}"> Chekout Order</a></li>
</ul>
</div>
@endsection