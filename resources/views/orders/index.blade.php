@extends('layouts.app')

@section('content')
<div class="container">
@include('layouts.alert')
    <div class="row">
    <table class="table table-bordered">
    <thead>
        <tr>
        
        <th scope="col">Order Number</th>
        <th scope="col">Order Total Price</th>
        <th scope="col">Order Checkout</th>
        </tr>
    </thead>
    <tbody>
    @foreach($orders as $order)
        <tr data-order-id="{{$order->id}}">
        
        <td>{{$order->id}}</td>
        <td>{{$order->total_price}}</td>
        <td><span class="cellcontent">@if($order->checked==1)<i class = "fa color--fadegreen fa-check"></i>@else <i class = "fa color--fadebrown fa-times"> @endif</a></span></td>
      
        </tr>
    @endforeach
    </tbody>
    </table>
    </div>
</div>
@endsection