@extends('layouts.app')

@section('content')
<div class="container">
@include('layouts.alert')
<div class="row">
    @if(auth()->user()->getAdminRole()==1)
    <div class="col-md-2">
    <a href="{{route('item.create')}}"><button type="button" class="btn btn-secondary"> Add Item</button></a>
    </div>
    @endif
    <div class="col-md-3">
    <a href="{{route('order.bill')}}" class="btn btn-info"> Show Order Bill</a>
    </div>
</div>
<div class="row">
<table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Item Name</th>
      <th scope="col">Item Price</th>
      @if(auth()->user()->getAdminRole()==1)
      <th scope="col">Actions</th>
      @endif
    </tr>
  </thead>
  <tbody>
  @foreach($items as $item)
    <tr data-item-id="{{$item->id}}">
      <th scope="row"><span class="cellcontent"><input type="checkbox" name="select-all" id="select-all" /></span></th>
      <td>{{$item->name}}</td>
      <td>{{$item->price}}</td>
      @if(auth()->user()->getAdminRole()==1)
      <td><span class="cellcontent"><a href= "{{route('item.edit',$item->id)}}" ,  class= "action-btn bgcolor--fadegreen color--white "><i class = "fa  fa-pencil"></i></a><a   class= "btn-warning-cancel action-btn bgcolor--fadebrown color--white "><i class = "fa  fa-trash-o"></i></a></span></td>
      @endif
    </tr>
  @endforeach
  </tbody>
</table>
</div>
<div class="row">

<div class="col-md-3">
<a class="btn btn-primary btn-make-order" href="#"> Make Order</a>
</div>
<div class="col-md-3">
<a class="btn btn-info" href="{{route('order.list')}}">Show Latest Orders</a>
</div>
</div>
</div>
@endsection
@section('js')
<script>
 
  $('.btn-warning-cancel').click(function(){
          var item_id = $(this).closest('tr').attr('data-item-id');
          var _token = '{{csrf_token()}}';
          
             $.ajax({
               type:'DELETE',
               url:'{{url("item/delete")}}'+'/'+ item_id,
               data:{_token:_token},
               success:function(data){
                $('tr[data-item-id='+item_id+']').fadeOut();
                swal("Deleted!", "Deleted successfully", "success");
              }
            });
             
          
        
    });

    $('.btn-make-order').click(function(){
          var selectedIds = $("input:checkbox:checked").map(function(){
            return $(this).closest('tr').attr('data-item-id');
          }).get();
          var _token = '{{csrf_token()}}';
         
             $.ajax({
               type:'POST',
               url:'{{route("order.store")}}',
               data:{ids:selectedIds,_token:_token},
               success:function(data){
                
                swal("Order Created Successfully!", "Your Data is transfered to bill", "success");
              }
            });
    });
             
           
       
        
  
  </script>

  

@endsection