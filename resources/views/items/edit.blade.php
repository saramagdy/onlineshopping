@extends('layouts.app')

@section('content')
<div class="container">
<form action="{{route('item.update', $item->id)}}" method="post">
{{csrf_field()}}
  <div class="form-group">
    <label for="exampleInputEmail1">Item Name</label>
    <input type="text" class="form-control" id="name"  placeholder="Enter Name" name="name" value="{{$item->name}}">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Item Price</label>
    <input type="number" class="form-control" id="price" placeholder="Price" name="price" value="{{$item->price}}">
  </div>
  <button type="submit" class="btn btn-primary">Edit</button>
</form>
</div>
@endsection