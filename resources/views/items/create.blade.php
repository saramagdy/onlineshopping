@extends('layouts.app')

@section('content')
<div class="container">
<form action="{{route('item.store')}}" method="post">
{{csrf_field()}}
  <div class="form-group">
    <label for="exampleInputEmail1">Item Name</label>
    <input type="text" class="form-control" id="name"  placeholder="Enter Name" required name="name">
    <span class="master_message color--fadegreen">
        @if ($errors->has('name'))
        {{ $errors->first('name')}}
        @endif
    </span> 
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Item Price</label>
    <input type="number" class="form-control" id="price" placeholder="Price" required name="price">
    <span class="master_message color--fadegreen">
        @if ($errors->has('price'))
        {{ $errors->first('price')}}
        @endif
    </span> 
  </div>
  <button type="submit" class="btn btn-primary">Add</button>
</form>
</div>
@endsection