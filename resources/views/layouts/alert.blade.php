@if(\session('success'))
<div class="alert alert-success">
{{\session('success')}}
</div>
@else(\session('error'))
<div class="alert alert-warning">
{{\session('error')}}
</div>
@endif