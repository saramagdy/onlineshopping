<?php

namespace App\Http\Controllers;
use Session;
use Validator;
use Illuminate\Http\Request;
use App\Models\Item;
use App\Models\Order;
use App\Libraries\Helpers;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['orders'] = Order::orderBy('created_at','desc')->where('user_id',\Auth::user()->id)->limit(5)->get();
       
        if(count($data['orders']) == 0 )
        {
            Session::flash('error', 'No Orders Found');
            return redirect()->route('item.list');
        }
        return view('orders.index', $data);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ids = $_POST['ids'];
        $total_price = 0;
        foreach ($ids as $id) {
            $item = Item::find($id);
            $total_price += $item->price;
        }
        Order::create([
            'user_id' => \Auth::user()->id,
            'total_price' => $total_price
        ])->items()->attach($ids);
        return redirect()->route('item.list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Checkout order
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function checkout($id)
    {
        $order = Order::find($id);
        if($order)
        {
            $order->update([
                'checked' => 1
            ]);
            try{
                Helpers::mail(\Auth::user()->name , \Auth::user()->email);
            }
            catch(\Exception $e)
            {
                Session::flash('error', 'Error while sending Email');
            }
            
        }
        else
        {
            Session::flash('error', 'Order Not Found');
            return redirect()->back();
        }
        Session::flash('success', 'Checkout  Successfully');
        return redirect()->route('item.list');
    }

    /**
     * SHow last order
     */

    public function bill()
    {
        $data['order'] = Order::where('checked',0)->orderBy('created_at','desc')->first();
        if(! $data['order'])
        {
            Session::flash('error', 'All Orders Checkout');
            return redirect()->route('item.list');
        }
        return view('orders.bill', $data);
    }
}
