<?php

namespace App\Http\Controllers;
use Session;
use Validator;
use Illuminate\Http\Request;
use App\Models\Item;
class ItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['items'] = Item::all();
        return view('items.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('items.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name'  => 'required',  
            'price'      => 'required',  
        ]);

        if ($validator->fails()) {
            return redirect()->back()
              ->withErrors($validator)
              ->withInput();
          }

        Item::create([
            'name' => $request->name,
            'price' => $request->price
        ]);
        Session::flash('success', 'Item Added Successfully');
        return redirect('/item/list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['item'] = Item::find($id);
        return view('items.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'name'  => 'required',  
            'price'      => 'required',  
        ]);

        if ($validator->fails()) {
            return redirect()->back()
              ->withErrors($validator)
              ->withInput();
          }
        $item = Item::find($id);
        if($item)
        {
            $item->update([
                'name' => $request->name,
                'price' => $request->price
            ]);

        }
        else
        {
        Session::flash('error', 'Item Not Found');
        return redirect()->back();
        }

        
        Session::flash('success', 'Item Updated Successfully');
        return redirect('/item/list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Item::find($id);
        if($item)
        {
            $item->delete();

        }
        else
        {
        Session::flash('error', 'Item Not Found');
        return redirect()->back();
        }

        Session::flash('success', 'Item deleted Successfully');
        return response('success');
    }
}
