<?php

namespace App\Http\Middleware;

use Closure;
use Session;


class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        foreach(auth()->user()->roles as $role)
        {
            if($role->pivot_role_id == 1)
            {
                return $next($request);
            }
           
        }
        
        
            
        
        Session::flash('error', 'You donot have permission to make this operation');
        return redirect()->guest('/home');
        
    }
}
