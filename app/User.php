<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'mobile', 'master_number'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Check Roles
     */
    public static function IsAdmin(){
        return  static::whereHas('roles', function ($q) {
             $q->Where('role_id', 1);
         })->first();
     }

     public static function IsUser(){
        return  static::whereHas('roles', function ($q) {
             $q->Where('role_id', 2);
         })->first();
     }

     public function getAdminRole(){
        foreach($this->roles as $role){
          if($role->pivot->role_id==1){
            return $role->pivot->role_id;
          }
        }
      }

    /**
     * Relations
     */

     /**
      * Roles
      */

      public function roles(){
          return $this->belongsToMany('App\Models\Role','user_roles', 'user_id', 'role_id');
      }

      public function orders(){
          return $this->hasMany('App\Models\Order');
      }
}
