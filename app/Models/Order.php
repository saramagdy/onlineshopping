<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $fillable = ['user_id', 'total_price', 'checked'];


    public $timestamps=true;

    /**Relations */
    public function items()
    {
        return $this->belongsToMany('App\Models\Item','order_items', 'order_id', 'item_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
