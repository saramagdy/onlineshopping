<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = 'items';
    protected $fillable = ['name', 'price'];


    public $timestamps=true;

    /**Relations */
    public function order()
    {
        return $this->belongsToMany('App\Models\Order','order_items', 'item_id', 'order_id');
    }
}
