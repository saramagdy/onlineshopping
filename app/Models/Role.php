<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';
    protected $fillable = ['name'];


    public $timestamps=false;

    /**Relations */
    public function user()
    {
        return $this->belongsToMany('App\User','user_roles', 'role_id', 'user_id');
    }
}
